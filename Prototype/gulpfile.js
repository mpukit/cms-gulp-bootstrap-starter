var gulp = require('gulp');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream'); // Converts Browserify stream to a format that can be consumed by other Gulp plugins
var browserify = require('browserify');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');
var fileinclude = require('gulp-file-include');
var inject = require('gulp-inject');
//var replace = require('gulp-replace'); *TODO
var imagemin = require('gulp-imagemin');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();

/* --------------------------------------------------------------
INJECT (Includes, JS, CSS)
--------------------------------------------------------------- */
gulp.task('inject', function() {
  return gulp.src('./assets/layouts/src/**/*.html')
    // Includes
    .pipe(fileinclude({
      prefix: '@@',
      basepath: './assets/includes/**'
    }))
    .pipe(gulp.dest('./assets/layouts/dist'))
    //.pipe(notify({ message: "File include tasks done."}) )
    // JS (min bundle for prod)
    .pipe(inject(
      gulp.src('./assets/client/js/dist/client.bundle.js', { read: false }), { relative: true }))
    .pipe(gulp.dest('./assets/layouts/dist'))
    //.pipe(notify({ message: "JS injected to layouts." }))
    // CSS (min bundle for prod)
    .pipe(inject(
      gulp.src('./assets/client/css/main.css', { read: false }), { relative: true }))
    .pipe(gulp.dest('./assets/layouts/dist'))
    //.pipe(notify({ message: "CSS injected to layouts." }))
    .pipe(browserSync.stream());
});

/* --------------------------------------------------------------
CSS (Compile CSS, Add Sourcemaps, Autoprefix)
--------------------------------------------------------------- */
gulp.task('css', function () {
  return gulp.src('./assets/client/css/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(plumber())
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(concat('main.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/client/css'))
    //.pipe(notify({ message: "Sass compiled, Sourcemaps created."}) )
    .pipe(browserSync.stream());
});

/* --------------------------------------------------------------
CSS (Minify CSS)
--------------------------------------------------------------- */
gulp.task('minify-css', () => {
  return gulp.src('./assets/client/css/main.css')
    .pipe(cleanCSS({compatibility: '*'}))
    .pipe(concat('main.min.css'))
    //.pipe(notify({ message: "CSS Minified."}) )
    .pipe(gulp.dest('./assets/client/css'));
});

/* --------------------------------------------------------------
JS (Bundle using Browserify, Hint, Compress)
--------------------------------------------------------------- */
gulp.task('js', function () {
  browserify('./assets/client/js/main.js')
    .bundle() 
    .on('error', function (e) { 
      gutil.log(e); 
    })
    .pipe(source('client.bundle.js')) 
    .pipe(gulp.dest('./assets/client/js/dist'))
    //.pipe(notify({ message: "JS bundled."}) )
    .pipe(browserSync.stream());
});

gulp.task('hint', function () {
  return gulp.src(['./assets/client/js/dist/client.bundle.js'])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('compress', function() {
  return gulp.src('./assets/client/js/dist/client.bundle.js')
    .pipe(uglify())
    .pipe(concat('client.bundle.min.js'))
    .pipe(gulp.dest('./assets/client/js/dist'))
    //.pipe(notify({ message: "JS compressed."}) )
});

/* --------------------------------------------------------------
IMAGES (Optimize Images)
--------------------------------------------------------------- */
gulp.task('images', function() {
  return gulp.src('./assets/client/images/src/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./assets/client/images/dist'))
    //.pipe(notify({ message: "Images compressed."}) )
});

/* --------------------------------------------------------------
BROWSER-SYNC (Reload Browser)
--------------------------------------------------------------- */
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./assets/"
        }
    });
});

/* --------------------------------------------------------------
WATCH (Tasks)
--------------------------------------------------------------- */
gulp.task('watch', function() {
  // HTML
  gulp.watch('./assets/**/*.html', ['inject']);
  // CSS
  gulp.watch('./assets/client/css/**/*.scss', ['css']);
  // JS
  // gulp.watch('./assets/client/js/**/*.js', ['js']); *TODO: Windows Browser Reload Issues Fix
});

/* --------------------------------------------------------------
GULP TASK (Serve)
--------------------------------------------------------------- */
gulp.task('serve', function() {
    gulp.start('css', 'minify-css', 'js', 'inject', 'compress', 'images', 'browser-sync', 'watch');
});
